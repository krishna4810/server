import dotenv from 'dotenv';
import { IMQServer } from "imqprotocol";
import { container, setup } from './di-setup';
import { PublisherHandler } from './presentation/handlers/publisherHandler';
import { SubscriberHandler } from './presentation/handlers/subscriberHandler';
setup();

// load the environment variables from the .env file
dotenv.config({
    path: '.env'
});
async function startServer() {
    const port = Number(process.env.APP_PORT);
    const host: string = process.env.APP_HOST as string;
    const server = new IMQServer().createServer(host, port);
    const pubHandler = container.resolve('publisherHandler');
    const subHandler = container.resolve('subscriberHandler');
    pubHandler.registerEvents(server);
    subHandler.registerEvents(server);
}

startServer();

