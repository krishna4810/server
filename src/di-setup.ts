const awilix = require('awilix');
import { PublisherService } from './application/services/publisherService';
import { SubscriberService } from './application/services/subscriberService';
import { TopicService } from './application/services/topicService';
import { Server } from './server';
import { Logger } from './Infrastructure/logger';
import { DLQRepository } from './Infrastructure/repositories/DLQRepository';
import { MessageRepository } from './Infrastructure/repositories/messageRepository';
import { PublisherRepository } from './Infrastructure/repositories/publisherRepository';
import { SubsciberRepository } from './Infrastructure/repositories/subscriberRepository';
import { SubscriberTopicMapRepository } from './Infrastructure/repositories/subscriberTopicMapRepository';
import { TopicRepository } from './Infrastructure/repositories/topicRepository';
import { PublisherHandler } from './presentation/handlers/publisherHandler';
import { SubscriberHandler } from './presentation/handlers/subscriberHandler';
import { PublisherValidator } from './presentation/requestValidators.ts/publisher';
import { SubscriberValidator } from './presentation/requestValidators.ts/subscriber';

export const container = awilix.createContainer({
    injectionMode: awilix.InjectionMode.CLASSIC
});

export function setup() {
    container.register({
        logger: awilix.asClass(Logger),
    });

    registerRepositories();
    registerServices();
    registerHandlers();
    registerValidators();
}

function registerRepositories() {
    container.register({
        messageRepo: awilix.asClass(MessageRepository),
        publisherRepo: awilix.asClass(PublisherRepository),
        subscriberRepo: awilix.asClass(SubsciberRepository),
        subscriberTopicMapRepo: awilix.asClass(SubscriberTopicMapRepository),
        topicRepo: awilix.asClass(TopicRepository),
        dlqRepo: awilix.asClass(DLQRepository),
    });
}

function registerServices() {
    container.register({
        topicService: awilix.asClass(TopicService),
        subscriberService: awilix.asClass(SubscriberService),
        publisherService: awilix.asClass(PublisherService),
    });
}

function registerHandlers() {
    container.register({
        publisherHandler: awilix.asClass(PublisherHandler),
        subscriberHandler: awilix.asClass(SubscriberHandler),
    });
}

function registerValidators() {
    container.register({
        publisherValidator: awilix.asClass(PublisherValidator),
        subscriberValidator: awilix.asClass(SubscriberValidator),
    });
}