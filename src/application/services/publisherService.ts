import { IMessage } from "../../core/domain/interfaces/iMessage";
import { ILogger } from "../../core/services/interfaces/ilogger";
import { IClientRepository } from "../../core/services/interfaces/repositories/iClientRepository";
import { IMessageRepository } from "../../core/services/interfaces/repositories/iMessageRepository";
import { ClientService } from "./clientService";

export class PublisherService extends ClientService {
    private logger: ILogger;
    private publisherRepo: IClientRepository;
    private messageRepo: IMessageRepository;

    constructor(messageRepo: IMessageRepository, publisherRepo: IClientRepository, logger: ILogger) {
        super(publisherRepo);
        this.logger = logger;
        this.publisherRepo = publisherRepo;
        this.messageRepo = messageRepo;
    }
    
    async publish(message: IMessage, topic: string) { 
        await this.messageRepo.insert(message, topic);
    }
}
