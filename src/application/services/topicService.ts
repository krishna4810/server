import { ILogger } from "../../core/services/interfaces/ilogger";
import { ITopicRepository } from "../../core/services/interfaces/repositories/iTopicRepository";

export class TopicService {
    private logger: ILogger;
    private topicRepo: ITopicRepository;
    private allTopicMap: any = null;

    constructor(topicRepo: ITopicRepository, logger: ILogger) {
        this.logger = logger;
        this.topicRepo = topicRepo;
    }
    
    async getAll(): Promise<string[]> {
        await this.ensureTopicsAreLoaded();
        return Object.keys(this.allTopicMap);
    }

    async isTopicAvailable(topicName: string) {
        await this.ensureTopicsAreLoaded();
        return !!this.allTopicMap[topicName];
    }

    async loadAllTopics() {
        let allTopics = await this.topicRepo.getAllTopics();
        this.allTopicMap = {}
        allTopics.forEach(topic => {
            this.allTopicMap[topic] = true;
        });
    }

    private async ensureTopicsAreLoaded() {
        if (!this.allTopicMap) {
            await this.loadAllTopics();
        }
    }
}