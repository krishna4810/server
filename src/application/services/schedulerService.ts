import { ILogger } from "../../core/services/interfaces/ilogger";
import { IDLQRepository } from "../../core/services/interfaces/repositories/iDLQRepository";
import { IMessageRepository } from "../../core/services/interfaces/repositories/iMessageRepository";
import { ITopicRepository } from "../../core/services/interfaces/repositories/iTopicRepository";
import { DLQRepository } from "../../Infrastructure/repositories/DLQRepository";

export class SchedulerService {
    private logger: ILogger;
    private messageRepo: IMessageRepository;
    private dlqRepo: IDLQRepository;
    private topicRepo: ITopicRepository;

    constructor(messageRepo: IMessageRepository, dlqRepo: IDLQRepository, topicRepo: ITopicRepository, logger: ILogger) {
        this.logger = logger;
        this.dlqRepo = dlqRepo;
        this.topicRepo = topicRepo;
        this.messageRepo = messageRepo;
    }

    async moveExpiredMessagesToDLQ() {
        let allTopics = await this.topicRepo.getAllTopics();
        for (let i = 0; i < allTopics.length; i++) {
            let expiredMessages = await this.getAndDeleteAllDeadMessages(allTopics[i]);
            await this.dlqRepo.add(expiredMessages, allTopics[i]);
        }
    }

    private async getAndDeleteAllDeadMessages(topic: string) {
        let currentDate = Date.now();
        let message = this.getAllDeadMessages(topic, currentDate);
        await this.deleteAllDeadMessages(topic, currentDate);
        return message;
    }
    private async deleteAllDeadMessages(topic: string, currentDate: any) {
        let query = {
            creationTime: { $lte: currentDate }
        }
        await this.messageRepo.delete(query, topic)
    }
    private async getAllDeadMessages(topic: string, currentDate: any) {
        let query = {
            creationTime: { $lte: currentDate }
        }
        return await this.messageRepo.getByQuery(query, topic);
    }
}
