import { IClient } from "../../core/domain/interfaces/iClient";
import { ISubscriberTopicMap } from "../../core/domain/interfaces/iSubscriberTopicMap";
import { ILogger } from "../../core/services/interfaces/ilogger";
import { IClientRepository } from "../../core/services/interfaces/repositories/iClientRepository";
import { IMessageRepository } from "../../core/services/interfaces/repositories/iMessageRepository";
import { SubscriberTopicMapRepository } from "../../Infrastructure/repositories/subscriberTopicMapRepository";
import { ClientService } from "./clientService";

export class SubscriberService extends ClientService {
  private logger: ILogger;
  private subscriberRepo: IClientRepository;
  private messageRepo: IMessageRepository;
  private subscriberTopicMapRepo: SubscriberTopicMapRepository;

  constructor(
    messageRepo: IMessageRepository,
    subscriberTopicMapRepo: SubscriberTopicMapRepository,
    subscriberRepo: IClientRepository,
    logger: ILogger
  ) {
    super(subscriberRepo);
    this.logger = logger;
    this.subscriberRepo = subscriberRepo;
    this.subscriberTopicMapRepo = subscriberTopicMapRepo;
    this.messageRepo = messageRepo;
  }

  async subscribe(email: string, topic: string) {
    let newSubsciberMap: ISubscriberTopicMap = {
      _id: null,
      topicName: topic,
      subscriberEmail: email,
      lastVisitedTime: Date.now(),
    };
    return await this.subscriberTopicMapRepo.add(newSubsciberMap);
  }
  async isSubscribed(email: string, topic: string) {
    let result = await this.subscriberTopicMapRepo.getByQuery({
      topicName: topic,
      subscriberEmail: email,
    });
    return !!result?.length;
    
  }

  async unSubscribe(email: string, topic: string) {
    await this.subscriberTopicMapRepo.delete(email, topic);
  }

  async pullMessages(email: string, topic: string) {
    let lastVisitedTime = this.getLastVisitTime(email, topic);
    let messages = await this.getAllMessagesForUser(topic, lastVisitedTime);
    return messages;
  }

  async getAllMessagesForUser(topic: string, lastVisitedTime: any) {
    let query = {
      creationTime: { $gt: lastVisitedTime },
    };
    let messages = await this.messageRepo.getByQuery(query, topic);
    let currentTime = Date.now();
    messages = messages.filter(
      (message) => message.expirationTime < currentTime
    );
  }

  async getLastVisitTime(email: string, topic: string) {
    let result = await this.subscriberTopicMapRepo.getByQuery({
      topicName: topic,
      subscriberEmail: email,
    });
    if (result?.length) {
      let subTopicMap = result[0];
      let lastVisitedTime = subTopicMap.lastVisitedTime;
      return lastVisitedTime;
    } else {
      throw new Error(
        "error while fetching message please check if you have subscibed the topic or not"
      );
    }
  }

  async updateLastVisitTime(email: string, topic: string) {
    await this.subscriberTopicMapRepo.updateLastVisitedTime(
      email,
      topic,
      Date.now()
    );
  }

  async getAllsubscribedTopics(email: string) {
    let result = await this.subscriberTopicMapRepo.getByQuery({
      subscriberEmail: email,
    });

    let allSubsribedTopics = result.map((subTopicMap) => subTopicMap.topicName);
    return allSubsribedTopics;
  }
}
