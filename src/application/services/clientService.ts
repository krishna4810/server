import { IClient } from "../../core/domain/interfaces/iClient";
import { IClientRepository } from "../../core/services/interfaces/repositories/iClientRepository";

export abstract class ClientService {
    private clientRepo: IClientRepository;
    constructor(clientRepo: IClientRepository) {
        this.clientRepo = clientRepo;
    }
    async doesExists(email: string) {
        let isSubscriberExist = false;
        if (email) {
            let result = await this.clientRepo.getByQuery({ email });
            if (result.length) {
                isSubscriberExist = true;
            }

        }
        return isSubscriberExist;
    }

    async register(email: string, name: string) {
        let newClient: IClient = {
            _id: null,
            email,
            name
        }
        this.clientRepo.add(newClient)

    }
}