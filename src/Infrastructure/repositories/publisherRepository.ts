import { IClient } from '../../core/domain/interfaces/iClient';
import { IClientRepository } from '../../core/services/interfaces/repositories/iClientRepository';
import { MongoDb } from '../db/mongodb';
import { formatResponse } from '../db/utility';
import { config } from '../db/config';

export class PublisherRepository implements IClientRepository {
    async add(client: IClient) {
        let publisherCollection = await this.getPublisherCollection();
        let result = await publisherCollection.insertOne(client);
        if (result) {
            let data = formatResponse(result);
            return data[0];
        } else {
            throw new Error("Error Occurred while Adding client data");
        }
    }

    async getByID(_id: any): Promise<IClient> {
        let publisherCollection = await this.getPublisherCollection();
        let result = await publisherCollection.find({ _id }).toArray();
        if (result) {
            if (result && result[0]) {
                return result[0];
            } else {
                return result;
            }
        } else {
            throw new Error("Error Occurred while Getting client data");
        }
    }

    async getByQuery(query: any): Promise<IClient[]> {
        let publisherCollection = await this.getPublisherCollection();
        let result = await publisherCollection.find(query).toArray();
        if (result) {
            return result;
        } else {
            throw new Error("Error Occurred while Getting client data");
        }
    }

    private async getPublisherCollection() {
        let connection = await MongoDb.getConnection();
        let db = connection.db(config.DATABASES.CLIENT.NAME);
        return db.collection(config.DATABASES.CLIENT.COLLECTIONS.PUBLISHER);
    }
}