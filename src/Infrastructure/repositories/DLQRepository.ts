import { IMessage } from '../../core/domain/interfaces/iMessage';
import { IDLQRepository } from '../../core/services/interfaces/repositories/iDLQRepository';
import { config } from '../db/config';
import { MongoDb } from '../db/mongodb';
import { formatResponse } from '../db/utility';

export class DLQRepository implements IDLQRepository {
    async add(messages: IMessage[], topic:string): Promise<any> {
        let messageCollection = await this.getDeadMessageCollectionByTopicName(topic);
        let result = await messageCollection.insertMany(messages);
        if (result) {
            let data = formatResponse(result);
            return data[0];
        } else {
            throw new Error("Error Occurred while inserting message data");
        }
    }

    async getByQuery(query: any, topic: string) {
        let deadMessageCollection = await this.getDeadMessageCollectionByTopicName(topic);
        let result = await deadMessageCollection.find(query).toArray();
        if (result) {
            return result;
        } else {
            throw new Error("Error Occurred while Getting client data");
        }
    }

    private async getDeadMessageCollectionByTopicName(topic: string) {
        let connection = await MongoDb.getConnection();
        let db = connection.db(config.DATABASES.DLQ.NAME);
        return db.collection(topic);
    }

}