import { IMessage } from '../../core/domain/interfaces/iMessage';
import { IMessageRepository } from '../../core/services/interfaces/repositories/iMessageRepository';
import { MongoDb } from '../db/mongodb';
import { formatResponse } from '../db/utility';
import { config } from '../db/config';

export class MessageRepository implements IMessageRepository {

    async insert(message: IMessage, topic: string): Promise<any> {
        let messageCollection = await this.getMessageCollectionByTopicName(topic);
        let result = await messageCollection.insertOne(message);
        if (result) {
            let data = formatResponse(result);
            return data[0];
        } else {
            throw new Error("Error Occurred while inserting message data");
        }
    }

    async getByQuery(query: any, topic: string) {
        let messageCollection = await this.getMessageCollectionByTopicName(topic);
        let result = await messageCollection.find(query).toArray();
        if (result) {
            return result;
        } else {
            throw new Error("Error Occurred while Getting client data");
        }
    }

    async delete(query: any, topic: string): Promise<any> {
        let messageCollection = await this.getMessageCollectionByTopicName(topic);
        await messageCollection.deleteMany(query).toArray();
        return
    }

    private async getMessageCollectionByTopicName(topic: string) {
        let connection = await MongoDb.getConnection();
        let db = connection.db(config.DATABASES.MESSAGE.NAME);
        return db.collection(topic);
    }

}