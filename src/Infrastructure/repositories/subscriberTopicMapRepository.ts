import { ISubscriberTopicMap } from '../../core/domain/interfaces/iSubscriberTopicMap';
import { ISubscriberTopicMapRepository } from '../../core/services/interfaces/repositories/iSubscriberTopicMapRepository';
import { MongoDb } from '../db/mongodb';
import { formatResponse } from '../db/utility';
import { config } from '../db/config';

export class SubscriberTopicMapRepository implements ISubscriberTopicMapRepository {
    async add(subscriberTopicMap: ISubscriberTopicMap): Promise<ISubscriberTopicMap> {
        let subscriberTopicMapCollection = await this.getSubscriberTopicMapCollection();
        let result = await subscriberTopicMapCollection.insertOne(subscriberTopicMap);
        if (result) {
            let data = formatResponse(result);
            return data[0];
        } else {
            throw new Error("Error Occurred while Adding client data");
        }
    }

    async delete(email: string, topic: string): Promise<any> {
        let subscriberTopicMapCollection = await this.getSubscriberTopicMapCollection();
        let result = await subscriberTopicMapCollection.deleteOne({
            topicName: topic,
            subscriberEmail: email,
        })
        if (result) {
            return result;
        } else {
            throw new Error("Error Occurred while Adding client data");
        }
    }

    async updateLastVisitedTime(subscriberEmail: string, topicName: string, time: any) {
        let subscriberTopicMapCollection = await this.getSubscriberTopicMapCollection();
        let query = {
            subscriberEmail,
            topicName
        }
        let result = await subscriberTopicMapCollection.findOneAndUpdate(query, { $set: { "lastVisitedTime": time } },
            { returnOriginal: false });
        if (result.value) {
            return result.value;
        } else {
            throw new Error("Error Occurred while Updating data");
        }
    }

    async getByQuery(query: any): Promise<ISubscriberTopicMap[]>  {
        let subscriberTopicMapCollection = await this.getSubscriberTopicMapCollection();
        let result = await subscriberTopicMapCollection.find(query).toArray();
        if (result) {
            return result;
        } else {
            throw new Error("Error Occurred while Getting client data");
        }
    }

    private async getSubscriberTopicMapCollection() {
        let connection = await MongoDb.getConnection();
        let db = connection.db(config.DATABASES.CLIENT.NAME);
        return db.collection(config.DATABASES.CLIENT.COLLECTIONS.SUBSCRIBER_TOPIC_MAP);
    }
}  