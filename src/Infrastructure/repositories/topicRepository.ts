import { ITopicRepository } from '../../core/services/interfaces/repositories/iTopicRepository';
import { MongoDb } from '../db/mongodb';
import { config } from '../db/config';

export class TopicRepository implements ITopicRepository {
    async getAllTopics(): Promise<string[]> {
        let connection = await MongoDb.getConnection();
        let db = connection.db(config.DATABASES.MESSAGE.NAME);
        let result = await db.listCollections().toArray();
        if (result) {
            let collectionNames = result.map((collectionData: { name: any; }) => collectionData.name);
            return collectionNames;
        } else {
            throw new Error("Error Occurred while fetching topic list");
        }
    }
} 