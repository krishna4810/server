import { ILogger } from '../core/services/interfaces/ilogger';

export class Logger implements ILogger{
    info(message: string): void {
        console.log(`${message}`);
    }
    warn(message: string): void {
        console.warn(`${message}`);
    }
    error(message: string): void {
        console.error(`${message}`);
    }
    debug(message: string): void {
        console.debug(`${message}`);
    }

}