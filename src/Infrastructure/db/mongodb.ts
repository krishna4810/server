const MongoClient = require("mongodb").MongoClient;

import { config } from './config';
export class MongoDb {
    private static client: any;
    private static connection = null;

    static async getConnection(): Promise<any> {
        MongoDb.ensureClientIsCreated();
        if (!MongoDb.connection) {
            MongoDb.connection = await MongoDb.client.connect();
        }
        return MongoDb.connection;
    }

    private static ensureClientIsCreated() {
        if (!MongoDb.client) {
            MongoDb.client = new MongoClient(config.URL,  { useUnifiedTopology: true });
        }
    }
}
