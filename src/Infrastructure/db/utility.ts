export function formatResponse(data: any) {
    if (data.hasOwnProperty("ops")) {
        return data.ops;
    } else {
        return data;
    }
}