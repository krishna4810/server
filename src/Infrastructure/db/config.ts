export const config = {
    "URL": "mongodb://localhost:27017",
    "DATABASES": {
        "CLIENT": {
            "NAME": "clients",
            "COLLECTIONS": {
                "PUBLISHER": "publisher",
                "SUBSCRIBER": "subscriber",
                "SUBSCRIBER_TOPIC_MAP": "subscriberTopicMap"
            }
        },
        "MESSAGE": {
            "NAME": "messages"
        },
        "DLQ": {
            "NAME": "dlq"
        }
    }
}