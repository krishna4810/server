import { EventTypes, IMQRequest, IMQResponse, PublisherRequestType, SubscriberRequestType } from "imqprotocol";
import { PublisherService } from "../../application/services/publisherService";
import { TopicService } from "../../application/services/topicService";
import { ILogger } from "../../core/services/interfaces/ilogger";
import { ResponseWriter } from "imqprotocol";
import { PublisherValidator } from "../requestValidators.ts/publisher";
import { IMessage } from "../../core/domain/interfaces/iMessage";
export class PublisherHandler {
    private publisherService: PublisherService;
    private topicService: TopicService;
    private logger: ILogger;
    private publisherValidator: PublisherValidator;
    constructor(publisherService: PublisherService, topicService: TopicService, logger: ILogger, publisherValidator: PublisherValidator) {
        this.publisherService = publisherService;
        this.topicService = topicService;
        this.logger = logger;
        this.publisherValidator = publisherValidator;
    }
    registerEvents(server: any) {
        server.on(EventTypes.PUBLISHER_EVENTS, async (req: IMQRequest, connection: any) => {
            try {
                await this.publisherValidator.validateRequest(req);
                let requestType = req.header.requestType;
                let response = new IMQResponse();
                switch (requestType) {
                    case PublisherRequestType.GET_ALL_TOPIC:
                        response.body.data = await this.topicService.getAll();
                        break;
                    case PublisherRequestType.PUBLISH:
                        let message: IMessage = {
                            _id: null;
                            author: req.header.email,
                            creationTime: Date.now(),
                            expirationTime: Date.now() + 10001,
                            data: req.body.data
                        }
                        await this.publisherService.publish(message, req.header.topic);
                        response.body.data = "published";
                        break;
                    case PublisherRequestType.REGISTER:
                        response.body.data = await this.publisherService.register(req.body.data.email, req.body.data.name);
                        response.body.data = "registered";
                        break;
                }

                ResponseWriter.write(connection, response)
            } catch (err) {
                let response: IMQResponse = new IMQResponse();
                response.header.statusCode = 500;
                response.body.data = {
                    error: "Error occured while processing your request: ::" + err.message
                }
                ResponseWriter.write(connection, response);
            }
        });
    }
}