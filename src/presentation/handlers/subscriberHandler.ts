import { EventTypes, IMQRequest, IMQResponse, PublisherRequestType, SubscriberRequestType } from "imqprotocol";
import { SubscriberService } from "../../application/services/subscriberService";
import { TopicService } from "../../application/services/topicService";
import { ILogger } from "../../core/services/interfaces/ilogger";
import { ResponseWriter } from "imqprotocol";
import { SubscriberValidator } from "../requestValidators.ts/subscriber";

export class SubscriberHandler {
    private subscriberService: SubscriberService;
    private topicService: TopicService;
    private logger: ILogger;
    private subscriberValidator: SubscriberValidator;
    constructor(subscriberService: SubscriberService, topicService: TopicService, logger: ILogger, subscriberValidator: SubscriberValidator) {
        this.subscriberService = subscriberService;
        this.topicService = topicService;
        this.logger = logger;
        this.subscriberValidator = subscriberValidator;
    }
    registerEvents(server: any) {
        server.on(EventTypes.PUBLISHER_EVENTS, async (req: IMQRequest, connection: any) => {
            try {
                await this.subscriberValidator.validateRequest(req);
                let requestType = req.header.requestType;
                let response = new IMQResponse();
                switch (requestType) {
                    case SubscriberRequestType.REGISTER:
                        response.body.data = await this.subscriberService.register(req.body.data.email, req.body.data.name);
                        response.body.data = "registered";
                        break;
                    case SubscriberRequestType.GET_ALL_TOPICS:
                        response.body.data = await this.topicService.getAll();
                        break;
                    case SubscriberRequestType.PULL:
                        response.body.data = await this.subscriberService.pullMessages(req.header.email, req.header.topic);
                        break;
                    case SubscriberRequestType.GET_SUBSCIBED_TOPICS:
                        response.body.data = await this.subscriberService.getAllsubscribedTopics(req.header.email);
                        break;
                    case SubscriberRequestType.SUBSCRIBE_TO_TOPIC:
                        response.body.data = await this.subscriberService.subscribe(req.header.email, req.header.topic);
                        break;
                    case SubscriberRequestType.AKNOWLEDGE:
                        response.body.data = await this.subscriberService.updateLastVisitTime(req.header.email, req.header.topic);
                        break;
                }
                response.header.statusCode = 200;
                ResponseWriter.write(connection, response)
            } catch (err) {
                let response: IMQResponse = new IMQResponse();
                response.header.statusCode = 500;
                response.body.data = {
                    error: "Error occured while processing your request: ::" + err.message
                }
                ResponseWriter.write(connection, response);
            }
        });
    }

}