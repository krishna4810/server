import { IMQRequest, PublisherRequestType } from "imqprotocol";
import { PublisherService } from "../../application/services/publisherService";
import { TopicService } from "../../application/services/topicService";
import { ILogger } from "../../core/services/interfaces/ilogger";

export class PublisherValidator{
    private publisherService: PublisherService;
    private topicService: TopicService;
    private logger: ILogger;
    constructor(publisherService: PublisherService, topicService: TopicService, logger: ILogger) {
        this.publisherService = publisherService;
        this.topicService = topicService;
        this.logger = logger;
    }
    async validateRequest(req: IMQRequest) {
        let requstType = req.header.requestType;
        let email = req.header.email;
        let isExistingUser = await this.publisherService.doesExists(email);
        switch (requstType) {
            case PublisherRequestType.REGISTER:
                if (isExistingUser) {
                    throw new Error("user is already registered");
                }
                break;
            case PublisherRequestType.PUBLISH:
                if (isExistingUser) {
                    throw new Error("please register youselfFirst");
                }
                await this.validateTopic(req.header?.topic)
                break;

            default:
                throw new Error("invalid request Type");
                break;
        }
    }

    async validateTopic(topic: any) {
        if (!(await this.topicService.isTopicAvailable(topic))) {
            throw new Error("requested topic is not available");
        };
    }
}