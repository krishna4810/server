import { IMQRequest, SubscriberRequestType } from "imqprotocol";
import { SubscriberService } from "../../application/services/subscriberService";
import { TopicService } from "../../application/services/topicService";
import { ILogger } from "../../core/services/interfaces/ilogger";

export class SubscriberValidator{
    private subscriberService: SubscriberService;
    private topicService: TopicService;
    private logger: ILogger;
    constructor(subscriberService: SubscriberService, topicService: TopicService, logger: ILogger) {
        this.subscriberService = subscriberService;
        this.topicService = topicService;
        this.logger = logger;
    }
    async validateRequest(req: IMQRequest) {
        let requstType = req.header.requestType;
        let email = req.header.email;
        let isExistingUser = await this.subscriberService.doesExists(email);
        switch (requstType) {
            case SubscriberRequestType.REGISTER:
                if (isExistingUser) {
                    throw new Error("user is already registered");
                }
                break;
            case SubscriberRequestType.SUBSCRIBE_TO_TOPIC
                || SubscriberRequestType.GET_SUBSCIBED_TOPICS
                || SubscriberRequestType.GET_ALL_TOPICS
                || SubscriberRequestType.AKNOWLEDGE
                || SubscriberRequestType.PULL:
                if (isExistingUser) {
                    throw new Error("please register youself first");
                }
            case SubscriberRequestType.SUBSCRIBE_TO_TOPIC
                || SubscriberRequestType.AKNOWLEDGE
                || SubscriberRequestType.PULL:
                await this.validateTopic(req.header?.topic);
                break;

            default:
                throw new Error("invalid request Type");
                break;
        }
    }
    async validateTopic(topic: any) {
        if (!(await this.topicService.isTopicAvailable(topic))) {
            throw new Error("requested topic is not available");
        };
    }
}