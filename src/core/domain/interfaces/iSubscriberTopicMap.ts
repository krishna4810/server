export interface ISubscriberTopicMap{
    _id: any;
    topicName: string;
    subscriberEmail: string,
    lastVisitedTime: any
}