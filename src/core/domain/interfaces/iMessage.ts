export interface IMessage{
    _id: any;
    author: string;
    creationTime: any;
    expirationTime:any
    data: string;
}