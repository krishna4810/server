import { IClient } from "../../../domain/interfaces/iClient";

export interface IClientRepository {
    add(client: IClient): Promise<any>;
    getByID(id: any): Promise<IClient>;
    getByQuery(query: any): Promise<Array<IClient>>;
}