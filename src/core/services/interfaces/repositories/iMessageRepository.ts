import { IMessage } from "../../../domain/interfaces/iMessage";

export interface IMessageRepository{
    insert(message: IMessage, topic: string): Promise<IMessage>;
    getByQuery(query: any, topic: string): Promise<Array<IMessage>>;
    delete(query: any, topic: string): Promise<any>;
}