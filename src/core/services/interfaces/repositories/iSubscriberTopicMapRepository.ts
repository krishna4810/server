import { ISubscriberTopicMap } from "../../../domain/interfaces/iSubscriberTopicMap";

export interface ISubscriberTopicMapRepository {
    add(subscriberTopicMap: ISubscriberTopicMap): Promise<any>
    updateLastVisitedTime(subscriberEmail: string, topicName: string, time: Date): Promise<any>;
    getByQuery(query: any): Promise<any>;
}