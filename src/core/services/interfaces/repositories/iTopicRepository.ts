

export interface ITopicRepository {
    getAllTopics(): Promise<string[]>;
}