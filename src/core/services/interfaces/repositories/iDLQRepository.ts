import { IMessage } from "../../../domain/interfaces/iMessage";


export interface IDLQRepository {
    add(messages: IMessage[], topic: string): Promise<any>;
    getByQuery(query: any, topic: string): Promise<Array<IMessage>>;
}